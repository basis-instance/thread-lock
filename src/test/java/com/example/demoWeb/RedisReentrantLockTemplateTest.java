package com.example.demoWeb;

import com.example.demoWeb.demo.Lock.DistributedReentranLockTemplate;
import com.example.demoWeb.demo.Lock.redislock.CallBack;
import com.example.demoWeb.demo.Lock.redislock.CounDown;
import com.example.demoWeb.demo.Lock.redislock.DistributedReentranLockTemplateImpl;
import redis.clients.jedis.JedisPool;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadLocalRandom;

public class RedisReentrantLockTemplateTest {

    public static void main(String [] args) throws InterruptedException {

        JedisPool jedisPool = new JedisPool("127.0.0.1",6379);
        DistributedReentranLockTemplate distributedReentranLockTemplate =  new DistributedReentranLockTemplateImpl(jedisPool);

        int size = 100;

        CountDownLatch countDownLatchBegin = new CountDownLatch(1);
        CountDownLatch countDownLatchEnd = new CountDownLatch(size);

        for (int i = 0; i < size ; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        countDownLatchBegin.await();
                    } catch (InterruptedException e) {
                        Thread.interrupted();
                    }
                    long sleepTime = ThreadLocalRandom.current().nextInt(5)*100;
                    distributedReentranLockTemplate.excute("test", 10000l, new CallBack() {
                        @Override
                        public void getLock() throws InterruptedException {
                            System.out.println(Thread.currentThread().getName() + " getLock");
                            Thread.sleep(sleepTime);
                            System.out.println(Thread.currentThread().getName() + ":sleep" + "sleepTime:" + sleepTime);
                            countDownLatchEnd.countDown();
                        }

                        @Override
                        public void timeOut() throws InterruptedException {
                            System.out.println(Thread.currentThread().getName() + " timeOut");
                            countDownLatchEnd.countDown();
                        }
                    });



                }
            }).start();
        }
        countDownLatchBegin.countDown();
        countDownLatchEnd.await();

    }




}
