package com.example.demoWeb;

import com.example.demoWeb.demo.Lock.DistributedReentrantLock;
import com.example.demoWeb.demo.Lock.redislock.RedisReentrantLock;
import redis.clients.jedis.JedisPool;

public class SimpleTest {

    public static void main(String [] args) {

        JedisPool jedisPool = new JedisPool("127.0.0.1",6379);

        DistributedReentrantLock distributedReentrantLock = new RedisReentrantLock(jedisPool,"订单");
        try {

            if(distributedReentrantLock.tryLock(1000)){
                System.out.println("拿到锁做业务操作");
            };
        }catch (Exception e){

        }finally {
            System.out.println("业务操作完后释放锁");
            distributedReentrantLock.unLock();
        }


    }
}
