package com.example.demoWeb.demo.Lock.redislock;

public interface CallBack {

    public void getLock() throws InterruptedException;

    public void timeOut() throws InterruptedException;
}
