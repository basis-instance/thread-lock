package com.example.demoWeb.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试类
 * @author liaowb
 */
@RestController
public class HelloWorldController {

    @RequestMapping("/holle")
    public String index() {

        return "holle world";
    }


}
